# Kubernetes Update
This repository holds all the commands and yaml files used to play around with various Kubernetes features.
The docker image used in the project can be found in the DevOps Mentor Project repository.

## Setup
To set up, clone the project repo mentioned above and run:
```
$ docker build -t <tag name> .
```

Since k8s will be setting up the containers for us, we don't need to run the image.
The next step is to set up the deployment yaml file and build the pods.

### Warning:
Whenever an image is referenced in the deployment file, k8s searches docker hub for the image.
So if we run `$ kubectl apply -f <.yaml file>` and then check our pods with `$ kubectl get pods`, we'll
be greeted with a 'ErrImagePull' status.

Since our project is on our local machine, we need to tell k8s to switch it's search location
to just our machine. To fix this, run:
```
$ minikube docker-env
$ eval $(minikube -p minikube docker-env)
```
This points the local docker daemon to minikube's internal docker registry which will now allow k8s
to pull images from our local machine.

## Deploying Pods
Now we run `$ kubectl apply -f <.yaml file>` to get our pods up and running.
We can check the status of our pods with `$ kubectl get pods`.

## Load Balancer
Kubernetes services features a load balancer which is able to distribute traffic to our pods.
To simulate this, a REST API was developed using Node and express. This project was then converted
into an image using docker and deployed using the deployment yaml file.

To test the load balancer, execute the following command to switch to the bash terminal of one of the pods:
```
$ kubectl get pods
$ kubectl exec -ti <pod_name> -- sh
```
Within one of the pod's shell, execute the following script:
```
# for i in $(seq <number_of_requests>); do
# curl <service_address>:3030/data;
# done
```
This will send a request to the API a number of times. The load balancer then balances the requests between our pods. The logs of the pod can be checked by using `$ kubectl logs <pod_name>`. The logs
of all 3 pods can be found in the text files under the *loadbalancer/kuber* folder.
