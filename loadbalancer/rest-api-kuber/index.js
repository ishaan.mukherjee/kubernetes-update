var express = require("express");
var app = express();

app.get("/data", (req, res, next) => {
	console.log("Data requested!");
	res.json({
		title: "Hello World",
		pages: 20,
		author: "ish",
	});
});

app.get("/", (req, res, next) => {
	res.json({
		error: "Try using the /data endpoint for data"
	});
});

app.listen(3000, () => {
	console.log("Server running on port 3000");
});


